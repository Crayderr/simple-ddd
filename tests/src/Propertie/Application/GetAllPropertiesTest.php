<?php

declare(strict_types=1);


namespace Test\Propertie\Application;

use App\Propertie\Application\GetAllProperties;
use App\Propertie\Infrastructure\PropertieRepositoryApiREST;
use PHPUnit\Framework\TestCase as TestCase;

class GetAllPropertiesTest extends TestCase
{
    private GetAllProperties $service;
    function setUp(): void
    {
        $this->service = new GetAllProperties(new PropertieRepositoryApiREST());
    }

    public function testGetAllProperties(): void
    {
        /**
         * Actions
         */
        $getAllProperties = $this->service->__invoke();

        /**
         * Asserts
         */
        $this->assertIsArray($getAllProperties);

        $expectedKeys = [
            'public_id',
            'title',
            'location'
        ];

        $firstProperty = $getAllProperties['content'][0];
        foreach ($expectedKeys as $key) {
            $this->assertArrayHasKey($key, $firstProperty);
        }
    }
}

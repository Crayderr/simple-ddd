<?php

declare (strict_types = 1);

namespace App\Propertie\Infrastructure;

use App\Propertie\Domain\PropertieRepository;
use GuzzleHttp\Client;

final class PropertieRepositoryApiREST implements PropertieRepository
{
    private const URL = 'https://api.stagingeb.com/v1/';
    public function __construct()
    {
        $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__ .'/../../../');
        $dotenv->load();
    }

    public function getAllPropierties(): array
    {
        $client = new Client();
        $res = $client->request('GET', $this->buildURI(['properties']), $this->buildOptions());

        if ($res->getStatusCode() !== 200) {
            throw new \Exception('Fail to get properties :('); //TODO: Create custom exception
        }

        return json_decode($res->getBody()->getContents(), true);
    }

    private function buildOptions(): array
    {
        return [
            'headers' => [
                'X-Authorization' => $_ENV['EB_API_KEY'],
                'accept'          => 'application/json',
            ],
        ];
    }

    private function buildURI(array $params): string
    {
        $uri = self::URL;
        foreach ($params as $param) {
            $uri .= $param . '/';
        }
        return $uri;
    }
}

<?php

declare (strict_types = 1);

namespace App\Propertie\Domain;

interface PropertieRepository
{
    public function getAllPropierties(): array;
}
<?php

declare(strict_types=1);

namespace App\Propertie\Application;

use App\Propertie\Domain\PropertieRepository;

class GetAllProperties
{
    public function __construct(
        private readonly PropertieRepository $propertieRepository,
    )
    {
    }

    public function __invoke(): array
    {
        return $this->propertieRepository->getAllPropierties();
    }
}

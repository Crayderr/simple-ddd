 # Simple DDD consumer for EasyBroker API

--
**Requirements**
 - install composer

* Run `composer install`

* Copy `.env.example` to `.env`
```.dotenv
# EasyBroker API Key:
# https://dev.easybroker.com/docs
EB_API_KEY=YOUR_EASYBROKER_API_KEY
```